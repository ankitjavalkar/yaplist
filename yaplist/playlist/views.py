import json

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import render_to_string
from django.template.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.forms.models import inlineformset_factory

from django.contrib.auth.models import User

from yaplist.playlist.models import Playlist, Item
from yaplist.playlist.forms import PlaylistForm, ItemForm, BaseItemFormSet, DeletePlaylistForm


def _playlists(request, playlists):
    paginator = Paginator(playlists, 10)
    page = request.GET.get('page')
    try:
        playlists = paginator.page(page)
    except PageNotAnInteger:
        playlists = paginator.page(1)
    except EmptyPage:
        playlists = paginator.page(paginator.num_pages)
    return render(request, 'playlist/playlists.html', {'playlists': playlists,})

@login_required
def playlists(request, username):
    user = get_object_or_404(User, username=username)
    all_playlists = Playlist.objects.all()
    return _playlists(request, all_playlists)

@login_required
def playlist(request, playlist_id):
    playlist = get_object_or_404(Playlist, id=playlist_id)
    playlist_items = playlist.get_playlist_items()
    item_preview = {}

    for idx, item in enumerate(playlist_items):
        item_preview[idx] = item.get_item_preview()

    jsonified_item_codes = playlist.get_jsonified_item_codes()

    return render(request, 'playlist/playlist.html',
        {'playlist': playlist,
            'item_codes': jsonified_item_codes,
            'item_preview': item_preview
        }
    )

@login_required
def create(request):
    ItemInlineFormSet = inlineformset_factory(Playlist, Item, fields=('link',), extra=1)

    if request.POST:
        playlist_form = PlaylistForm(request.POST)
        if playlist_form.is_valid():
            playlist = Playlist()
            playlist.title = playlist_form.cleaned_data.get('title')
            playlist.creator = request.user
            item_formset = ItemInlineFormSet(request.POST, request.FILES, instance=playlist)
            if item_formset.is_valid():
                playlist.save()
                item_formset.save()
                return redirect('/edit/{}'.format(playlist.id))

    else:
        playlist_form = PlaylistForm()
        item_formset = ItemInlineFormSet()

    return render(request, 'playlist/create.html',
        {'playlist_form': playlist_form,
            'item_formset': item_formset
        }
    )

@login_required
def edit(request, playlist_id):
    ItemInlineFormSet = inlineformset_factory(Playlist, Item, fields=('link',), extra=1) # fields='__all__'

    if playlist_id:
        playlist = get_object_or_404(Playlist, id=playlist_id)
    else:
        playlist = Playlist(creator=request.user)

    if request.POST:
        playlist_form = PlaylistForm(request.POST, instance=playlist)
        if playlist_form.is_valid():
            item_formset = ItemInlineFormSet(request.POST, request.FILES, instance=playlist)
            if item_formset.is_valid():
                playlist.save()
                item_formset.save()
                return redirect('/edit/{}'.format(playlist.id))
    else:
        playlist_form = PlaylistForm(instance=playlist)
        item_formset = ItemInlineFormSet(instance=playlist)

    return render(request, 'playlist/create.html',
        {'playlist_form': playlist_form,
            'item_formset': item_formset
        }
    )

@login_required
def delete(request, playlist_id):
    playlist = get_object_or_404(Playlist, id=playlist_id, creator=request.user)
    if request.method == 'POST':
        form = DeletePlaylistForm(request.POST, instance=playlist)

        if form.is_valid():
            playlist.delete()
            return redirect('/dashboard/')

    else:
        form = DeleteNewForm(instance=new_to_delete)

    template_vars = {'form': form}
    return redirect('/dashboard/')
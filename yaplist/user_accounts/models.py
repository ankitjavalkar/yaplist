from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from yaplist.playlist.models import Playlist


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    public_email = models.EmailField(null=True, blank=True)

    def get_playlists(self):
        playlists = Playlist.objects.filter(creator=self.user)
        return playlists

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

post_save.connect(create_user_profile, sender=User)
post_save.connect(save_user_profile, sender=User)
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from yaplist.user_accounts.forms import SignUpForm


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            email = form.cleaned_data.get('email')
            User.objects.create_user(username=username, password=password, email=email)
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.add_message(request, messages.SUCCESS, 'Congratulations! Your account was created successfully')
            return redirect('/dashboard/')
        else:
            messages.add_message(request, messages.ERROR, 'Your account could not be created. Please check if all fields have been filled correctly')
            return render(request, 'user_accounts/signup.html', { 'form': form })
    else:
        return render(request, 'user_accounts/signup.html', { 'form': SignUpForm })


def signin(request):
    if request.user.is_authenticated:
        return redirect('/dashboard/')
    else:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('/dashboard/')
                else:
                    messages.add_message(request, messages.ERROR, 'Your account has been deactivated')
                    return render(request, 'user_accounts/signin.html')
            else:
                messages.add_message(request, messages.ERROR, 'Username or password is incorrect')
                return render(request, 'user_accounts/signin.html')
        else:
            return render(request, 'user_accounts/signin.html')

def signout(request):
    logout(request)
    return redirect('/')


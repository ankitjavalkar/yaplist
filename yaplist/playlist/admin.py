from django.contrib import admin

from .models import Playlist, Item

admin.site.register(Playlist)
admin.site.register(Item)
from django import forms
from django.forms.models import BaseInlineFormSet

from yaplist.playlist.models import Playlist, Item


class PlaylistForm(forms.ModelForm):
    title = forms.CharField(
            widget=forms.TextInput(attrs={ 'class': 'form-control', 'placeholder': 'Title of Playlist' }), 
            max_length=255)

    class Meta:
        model = Playlist
        fields = ['title']

class DeletePlaylistForm(forms.ModelForm):
    class Meta:
        model = Playlist
        fields = []

class ItemForm(forms.ModelForm):
    link = forms.URLField()

    class Meta:
        model = Item
        fields = ['link']

class BaseItemFormSet(BaseInlineFormSet):
    def clean(self):
        """
        Adds validation to check that no two Playlist Items contain the same URL
        """
        if any(self.errors):
            return

        links = []
        duplicates = False

        for form in self.forms:
            if form.cleaned_data:
                link = form.cleaned_data['link']

                # Check that no two links have the same URL
                if link:
                    if link in links:
                        duplicates = True
                    links.append(link)

                if duplicates:
                    raise forms.ValidationError(
                        'Playlist Items must have unique URLs.',
                        code='duplicate_links'
                    )
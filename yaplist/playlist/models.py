from datetime import datetime
import json

from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from yaplist.playlist.extractors.extractors import detect_backend


class Playlist(models.Model):
    title = models.CharField(max_length=80)
    slug = models.SlugField(max_length=80, null=True, blank=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    create_date = models.DateTimeField(auto_now_add=True)
    editor = models.ForeignKey(User, blank=True, null=True, related_name='+', on_delete=models.CASCADE)
    edit_date = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs): # NEEDS FIX
        if not self.pk:
            super(Playlist, self).save(*args, **kwargs)
        else:
            self.edit_date = datetime.now()
        if not self.slug:
            slug_str = '{}'.format(self.title.lower())
            self.slug = slugify(slug_str)
        super(Playlist, self).save(*args, **kwargs)

    def get_playlist_items(self):
        items = self.item_set.order_by('-create_date')
        return items

    def get_jsonified_item_codes(self):
        item_codes = []
        items = self.get_playlist_items()
        for item in items:
            item_codes.append(
                {
                    'code': item.get_item_code(),
                    'service': item.get_item_service(),
                }
            )

        return json.dumps(item_codes)

    def __unicode__(self):
        return self.title


class Item(models.Model):
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    link = models.URLField()
    code = models.CharField(max_length=80, editable=False)
    title = models.CharField(max_length=130, editable=False)
    create_date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.code = self.get_item_code()
        self.title = self.get_item_title()
        super(Item, self).save(*args, **kwargs)

    @property
    def get_item_extractor(self):
        return detect_backend(self.link)

    def get_item_service(self):
        return self.get_item_extractor.service

    def get_item_code(self):
        return self.get_item_extractor.code

    def get_item_title(self):
        return self.get_item_extractor.title

    def get_item_thumbnail(self):
        return self.get_item_extractor.thumbnail

    def get_item_preview(self):
        return {
            'thumbnail_url': self.get_item_thumbnail(),
            'code': self.get_item_code(),
            'title': self.get_item_title()
        }
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User

def home(request):
    if request.user.is_authenticated:
        return render(request, 'core/cover.html', {
             'logged_in': True,
             'user': request.user,
            }) # Template will show a Dashboard redirect button
    else:
        return render(request, 'core/cover.html')

def dashboard(request):
    user = get_object_or_404(User, username=request.user)
    if request.user.is_authenticated:
        user_playlists = request.user.profile.get_playlists()
        return render(request, 'core/dashboard.html', {
             'user_playlists': user_playlists,
             'user': user,
            })
    else:
        return redirect('/signin/')

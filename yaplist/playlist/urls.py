from django.urls import path

from yaplist.playlist import views as playlist_views


app_name = 'playlist'

urlpatterns = [
    path('new/', playlist_views.create, name='create'),
    path('edit/<int:playlist_id>/', playlist_views.edit, name='edit'),
    path('delete/<int:playlist_id>/', playlist_views.delete, name='delete'),
    path('play/<int:playlist_id>/', playlist_views.playlist, name='playlist'),
]

from django.urls import path

from yaplist.user_accounts import views as user_accounts_views


app_name = 'user_accounts'

urlpatterns = [
    path('signin/', user_accounts_views.signin, name='signin'),
    path('signout/', user_accounts_views.signout, name='signout'),
    path('signup/', user_accounts_views.signup, name='signup'),
]
